package com.seidor.port.inbound.api.controller;

import org.gitlab4j.api.GitLabApiException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.seidor.application.service.GitlabCommandService;
import com.seidor.domain.command.GitlabCommand;
import com.seidor.port.inbound.api.dto.GitlabDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/gitlab.localhost.com:9090")
public class GitlabController {

  @Autowired
  GitlabCommandService gitlabCommandService;

  @ApiOperation(value = "Create branches and tag in repository", response = String.class,
      tags = "createRepository")
  @ApiResponses(value = {@ApiResponse(code = 200, message = "Suceess|OK"),
      @ApiResponse(code = 401, message = "not authorized!"),
      @ApiResponse(code = 403, message = "forbidden!!!"),
      @ApiResponse(code = 404, message = "not found!!!"),
      @ApiResponse(code = 500, message = "Internal error"),})
  @PostMapping
  public ResponseEntity<String> createRepository(@RequestBody GitlabDto gitlabDto) {

    try {
      gitlabCommandService
          .createRepository(GitlabCommand.builder().name(gitlabDto.getName()).build());
    } catch (GitLabApiException e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return ResponseEntity.status(HttpStatus.OK).body("The repository was created.");

  }

}
