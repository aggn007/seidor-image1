package com.seidor.port.inbound.api.dto;

import javax.validation.constraints.Pattern;
import org.hibernate.validator.constraints.NotEmpty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class GitlabDto {

  @NotEmpty(message = "{name.missing}")
  @Pattern(regexp = "^[a-zA-Z\\s]*$", message = "{name.notAllow}")
  private String name;

}
