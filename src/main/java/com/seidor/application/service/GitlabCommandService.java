package com.seidor.application.service;

import org.gitlab4j.api.GitLabApiException;
import com.seidor.domain.command.GitlabCommand;

public interface GitlabCommandService {

  void createRepository(GitlabCommand gitlabCommand) throws GitLabApiException ;
}
