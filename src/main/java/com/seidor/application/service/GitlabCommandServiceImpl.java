package com.seidor.application.service;

import java.util.List;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.Tag;
import org.gitlab4j.api.models.Visibility;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.seidor.domain.command.GitlabCommand;

@Service
public class GitlabCommandServiceImpl implements GitlabCommandService {

  @Value("${url.gitlab}")
  private String url;

  @Value("${accessToken.gitlab}")
  private String accessToken;

  @Override
  public void createRepository(GitlabCommand gitlabCommand) throws GitLabApiException {

    GitLabApi gitLabApi = new GitLabApi(url, accessToken);

    // try {
    // Project project = gitLabApi.getProjectApi().createProject("proyecto1", null,
    // "GitLab4J test project.", true, true, true, true, Visibility.PUBLIC, null, null);
    // } catch (GitLabApiException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    //

    Pager<Project> projectPager = gitLabApi.getProjectApi().getOwnedProjects(10);
    boolean isExist = false;
    List<Project> projects = projectPager.next();

    for (Project project : projects) {
      if (project.getName().equals(gitlabCommand.getName())) {
        gitLabApi.getRepositoryApi().createBranch(project, "master", "main");
        gitLabApi.getRepositoryApi().createBranch(project, "develop", "main");
        gitLabApi.getTagsApi().createTag(project, "tag1", "main");
        isExist = true;
        break;
      }
      // Project forkedProject = gitLabApi.getProjectApi().forkProject(project.getId(),
      // project.getNamespace().getId());

    }

    if (!isExist)
      throw new GitLabApiException("The project doesn´t exist.");

    gitLabApi.close();

  }
}
