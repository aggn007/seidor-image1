# seidor-image1

Service that create a repository with two branches (master & develop), and one tag (0.0.1).

### Requeriments 

| Herramienta     | Version                           |
|:----------------|:----------------------------------|
| Java            | 11.x                              |
| Maven           |                                   |
| Lombok          | 1.x                               |

### Service context

- /seidor-demo/gitlab.localhost.com:9090 

### Service details

| Endpoint                                     | Descripcion                                       |
|:---------------------------------------------|:--------------------------------------------------|
| (POST) /gitlab.localhost.com:9090                             |Create branches and tag in repository


## Docker
Execute the following instructions:
- git clone https://gitlab.com/aggn007/seidor-image1.git
- cd seidor-image1
- mvn clean install
- docker build -t seidor-image1 .
- docker run -p 9090:8080 seidor-image1
- You can now access http://localhost:9090/seidor-demo/swagger-ui.html#!/ with a browser


